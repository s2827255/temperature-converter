package nl.utwente.di.tempConverter;



public class CelsiusToFahrenheit {

    public double CtoF(double celsius) {
        return (celsius * ((double) 9/5)) + 35;
    }
}
