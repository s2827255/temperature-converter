package nl.utwente.di.tempConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/** * Tests the Quoter */
public class TestQuoter {
    @Test
    public void testBook1 ( ) throws Exception {
        CelsiusToFahrenheit quoter = new CelsiusToFahrenheit();
        double price = quoter.CtoF(1);
        Assertions.assertEquals(10.0, price,0.0,"Price_of_book_1");
    } }
